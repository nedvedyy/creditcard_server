var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mysql       = require('mysql');
var routes = require('./routes/index');
var users = require('./routes/users');
var port = process.env.PORT || 8080;
var fs = require("fs");
var Memcached = require('memcached');
var memcached = new Memcached('creditcard-memcache.fr4b8j.0001.apse1.cache.amazonaws.com:11211', {
  retries:0,
  retry:3000,
  remove:true});


//AWS-SDK setup.
var AWS = require('aws-sdk');
AWS.config.update({region:'ap-southeast-1'});

var app = express();
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);

//basic setting
var serverIP    = 'creditcard.cbwiqcucvz8o.ap-southeast-1.rds.amazonaws.com';
var pool  = mysql.createPool({
  host     : serverIP,
  user     : 'admin',
  password : '5257sisi',
  database : 'creditcard',
  port     : '3306'
});

// global variables.
var dealsJson ;

//common functions

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
          Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

// API routing
// =============================================================================
var router = express.Router();              // get an instance of the express Router


router.get('/deals', function(req, res) {
  console.log('*********************************');
  console.log('HTTP GET:/deals:');
  // http://localhost:8080/api/deals?lat=1.276595&lng=103.844091
  // http://192.168.1.3:8080/api/deals?lat=1.276595&lng=103.844091
  /*
  memcached.gets('deals_json', function (err, data) {
    if(err)
      console.log(err);
    else
      console.log(data);
    // Please note that the data is stored under the name of the given key.
  });*/

  var lat       = req.query.lat; // test value: 1.276595
  var lng       = req.query.lng; // test value: 103.844091
  console.log('lat:'+lat);
  console.log('lng:'+lng);
  //sorting
  console.time('filter sorting deals');
  dealsJson.sort(function(a, b) {
    a.distance = getDistanceFromLatLonInKm(a.shopLat,a.shopLng,lat,lng);
    b.distance = getDistanceFromLatLonInKm(b.shopLat,b.shopLng,lat,lng);
    return a.distance - b.distance;
  });
  console.timeEnd('filter sorting deals');
  res.send(JSON.stringify(dealsJson));
});


router.get('/gen_deals', function(req, res) {
  console.log('*********************************');
  console.log('API: /gen_deals');
  var queryString = 'select dealItemId, dealDescription, dealImgLink,dealTerms,dealURL,dealExcerpt,dealStart,dealEnd, ' +
      'shopLat, shopLng, shopName, shopContact,shopAddress,shopPostal,shopMemberType, shopURL, ' +
      'typeName,typeValue,typeDisplayName, ' +
      'genreName,genreDescription,genreDisplayName ' +
      'from TBL_DEALS ' +
      'left join TBL_SHOPS ON TBL_SHOPS.shopId= (SELECT shopId FROM TBL_DEALS,TBL_DEALSHOP WHERE TBL_DEALS.dealItemId = TBL_DEALSHOP.dealId ) ' +
      'left join TBL_GENRES ON TBL_GENRES.genreId = (SELECT genreId FROM TBL_DEALS,TBL_DEALGENRE WHERE TBL_DEALS.dealItemId = TBL_DEALGENRE.dealId ) ' +
      'left join TBL_TYPES ON TBL_TYPES.typeId = (SELECT typeId FROM TBL_DEALS,TBL_DEALTYPE WHERE TBL_DEALS.dealItemId = TBL_DEALTYPE.dealId ) ' +
      'WHERE TBL_DEALS.dealStatus = "Y" AND TBL_DEALS.dealEnd >= CURDATE() AND TBL_GENRES.status = "Y" AND TBL_TYPES.status = "Y"';
  //console.log('queryString:');
  //console.log(queryString);
  pool.getConnection(function(err, connection) {
    connection.query( queryString, function(err, rows) {
      // And done with the connection.
      connection.release();
      if (err)
      {
        //throw err
        res.send(JSON.stringify({"message":'Errors in /gen_deals:'+err}));
      }
      else
      {
        if (rows.length > 0)
        {
          dealsJson = rows;
          //write to elastic cache.
          /*
          console.log('write dealsJson to cache:');
          memcached.set('deals_json', dealsJson, 10, function (err) {
            console.log(err);
          });*/
          res.send(JSON.stringify(dealsJson));
        }else{
          res.send(JSON.stringify({"message":'No data'}));
        }
      }
    });
  });
});

router.get('/types', function(req, res) {
  console.log('*********************************');
  console.log('HTTP GET:/types:');
  pool.getConnection(function(err, connection) {
    // Use the connection
    console.log(err);
    var queryString = "select * from TBL_TYPES";
    connection.query( queryString, function(err, rows) {
      connection.release();
      // And done with the connection.
      if (err)
      {
        //throw err
        res.send(JSON.stringify({"message":'Errors in /types:'+err}));
      }
      else
      {
        if (rows.length > 0)
        {
          res.send(JSON.stringify(rows));
        }
        else
        {
          res.send(JSON.stringify({"message":'TBL_TYPES No records', "code":'4'}));
        }
      }
    });
  });
});

router.get('/genres', function(req, res) {
  console.log('*********************************');
  console.log('HTTP GET:/genres:');
  pool.getConnection(function(err, connection) {
    // Use the connection
    console.log(err);
    var queryString = "select * from TBL_GENRES";
    connection.query( queryString, function(err, rows) {
      connection.release();
      // And done with the connection.
      if (err)
      {
        //throw err
        res.send(JSON.stringify({"message":'Errors in /genres:'+err}));
      }
      else
      {
        if (rows.length > 0)
        {
          res.send(JSON.stringify(rows));
        }
        else
        {
          res.send(JSON.stringify({"message":'TBL_GENRES No records', "code":'4'}));
        }
      }
    });
  });
});

router.get('/deal', function(req, res) {
  console.log('http get hit /deal');
  var dealItemId       = req.query.dealItemId;
  pool.getConnection(function(err, connection) {
    // Use the connection
    console.log(err);
    var queryString = "select dealItemId, dealTC, dealURL, shopContact as contactNumber from dealItem_table, shop_table where dealItemId=" + dealItemId
        + " and shop_table.shopId = dealItem_table.shopCode";
    connection.query( queryString, function(err, rows) {
      connection.release();
      // And done with the connection.
      if (err)
      {
        //throw err
        res.send(JSON.stringify({'Content-Type': 'application/json'}));
      }
      else
      {
        if (rows.length > 0)
        {
          res.send(JSON.stringify(rows));
        }
        else
        {
          res.send(JSON.stringify({"message":'caseDealItem No records', "code":'4'}));
        }
      }
    });
  });
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);



// START THE SERVER
// =============================================================================
// load Json Data
// Read Synchrously
console.time('loading sample JSON');
var loadingStartTime = new Date();
dealsJson = JSON.parse(fs.readFileSync('1000Records.json', 'utf8'));
console.timeEnd('loading sample JSON');

/*
console.log('memcached:touch deals_json');
memcached.touch('deals_json', 100, function (err) {
  console.log(err);
});*/

app.listen(port);
console.log('Nodejs happens on port ' + port);

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
