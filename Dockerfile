FROM node:argon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN echo "before npm install"
RUN npm install
RUN echo "after npm install"

# Bundle app source
COPY . /usr/src/app

EXPOSE 8080
RUN echo "before CMD"
CMD [ "npm", "start" ]
RUN echo "after CMD"
















